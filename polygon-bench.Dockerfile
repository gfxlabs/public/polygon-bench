# syntax=docker/dockerfile:1
FROM golang:1.17.5-alpine
RUN apk add build-base
WORKDIR /src
COPY common common
COPY go.mod go.mod
COPY go.sum go.sum
COPY Makefile Makefile

COPY app app/

RUN go mod tidy
RUN make apps

FROM alpine:latest
WORKDIR /root/
COPY --from=0 /src/bin .
CMD ["./collector.exe"]
