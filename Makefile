APPS = $(shell ls app)

tidy:
	go mod tidy
	echo $(APPS)

apps: tidy
	mkdir -p bin
	for dir in $(APPS); do\
		go build -o ./bin/$$dir.exe ./app/$$dir/*; \
	done

clean:
	rm -rf bin

docker: 
	DOCKER_BUILDKIT=1 docker build -f polygon-bench.Dockerfile -t gfxlabs/polygon-bench .
