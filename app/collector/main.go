package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"sync"
	"time"

	"gfxlabs/polygon-bench/common"
	"gfxlabs/polygon-bench/common/arango"

	"github.com/arangodb/go-driver"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/jessevdk/go-flags"
)

type Options struct {
	All    bool `short:"A" long:"all" description:"collect all stats"`
	Upload bool `short:"u" long:"upload" description:"use db"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

type BenchClient struct {
	name string
	c    *ethclient.Client
}

type Scope struct {
	clients map[string]*BenchClient
	db      *arango.ArangoStore

	collection driver.Collection
}

func main() {
	if _, err := parser.Parse(); err != nil {
		switch flagsErr := err.(type) {
		case flags.ErrorType:
			if flagsErr == flags.ErrHelp {
				os.Exit(0)
			}
			os.Exit(1)
		case nil:
		default:
			log.Println(err)
		}
	}

	scope := &Scope{
		clients: map[string]*BenchClient{},
	}

	var err error
	if options.Upload {
		scope.db, err = arango.NewArango(os.Getenv("ARANGO_ADDR"),
			fmt.Sprintf("%s:%s", os.Getenv("ARANGO_USER"), os.Getenv("ARANGO_PASS")),
		)
		if err != nil {
			log.Panicln(err)
		}
		err = scope.db.Migrate()
		if err != nil {
			log.Println(err)
		}
		scope.collection, err = scope.db.Collection(context.Background(), "datapoints")
		if err != nil {
			log.Println(err)
		}
		log.Println("here")
	}

	//clients
	scope.clients["polygon-rpc"] = &BenchClient{name: "polygon-rpc"}
	scope.clients["polygon-rpc"].c, err = ethclient.Dial("https://polygon-rpc.com")
	if err != nil {
		log.Println(err)
		delete(scope.clients, "polygon-rpc")
	}

	scope.clients["maticvigil"] = &BenchClient{name: "infura-public"}
	scope.clients["maticvigil"].c, err = ethclient.Dial("https://rpc-mainnet.maticvigil.com/")
	if err != nil {
		log.Println(err)
		delete(scope.clients, "magicvigil")
	}

	if os.Getenv("ALCHEMY_URL") != "" {
		scope.clients["alchemy-private"] = &BenchClient{name: "alchemy-private"}
		scope.clients["alchemy-private"].c, err = ethclient.Dial(os.Getenv("ALCHEMY_URL"))
		if err != nil {
			log.Println(err)
			delete(scope.clients, "polygon_private")
		}
	}

	if os.Getenv("INFURA_URL") != "" {
		scope.clients["infura-private"] = &BenchClient{name: "infura-private"}
		scope.clients["infura-private"].c, err = ethclient.Dial(os.Getenv("INFURA_URL"))
		if err != nil {
			log.Println(err)
			delete(scope.clients, "infura_private")
		}
	}

	log.Println("now querying")
	ctx := context.Background()
	for {
		wg := &sync.WaitGroup{}
		for k, v := range scope.clients {
			wg.Add(1)
			go func(k string, v *BenchClient) {
				defer wg.Done()
				start := time.Now()
				block, err := v.c.BlockNumber(ctx)
				if err != nil {
					log.Println(err)
					return
				}
				newPoint := BlockDatapoint{
					BlockNumber:  block,
					ResponseTime: time.Now().Sub(start),
				}
				newPoint.Snapshot()
				newPoint.Assign(k, "blocknumber")
				if scope.collection != nil {
					log.Println(newPoint)
					scope.collection.CreateDocument(ctx, newPoint)
				}
			}(k, v)
		}
		wg.Wait()
		time.Sleep(5 * time.Second)
	}
}

type BlockDatapoint struct {
	common.Datapoint
	BlockNumber  uint64        `json:"block_number"`
	ResponseTime time.Duration `json:"response_time"`
}
