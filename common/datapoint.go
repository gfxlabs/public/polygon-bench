package common

import "time"

type Datapoint struct {
	Bucket    string    `json:"bucket"`
	Bin       string    `json:"bin"`
	Timestamp time.Time `json:"ts"`
	Micros    uint64    `json:"micros"`
}

func (D *Datapoint) Assign(bucket, bin string) {
	D.Bucket = bucket
	D.Bin = bin
}

func (D *Datapoint) Snapshot() {
	D.Timestamp = time.Now()
	D.Micros = uint64(D.Timestamp.UnixMicro())
}
