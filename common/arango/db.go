package arango

import (
	"context"
	"crypto/tls"
	"strings"

	"github.com/arangodb/go-driver"
	"github.com/arangodb/go-driver/http"
)

type ArangoStore struct {
	driver.Database
	ctx context.Context
}

func (A *ArangoStore) Migrate() error {
	err := A.MigrateCollections()
	if err != nil {
		return err
	}
	return err
}

func (A *ArangoStore) MigrateCollections() error {
	ok, err := A.CollectionExists(A.ctx, "datapoints")
	if !ok {
		A.CreateCollection(A.ctx, "datapoints", nil)
	}
	coll, err := A.Collection(A.ctx, "datapoints")
	if err != nil {
		return err
	}

	idxs := []string{"bucket", "bin", "ts", "micros"}
	for _, v := range idxs {
		ok, _ = coll.IndexExists(A.ctx, "IDX_"+v)
		if !ok {
			_, _, err := coll.EnsurePersistentIndex(A.ctx, []string{v}, &driver.EnsurePersistentIndexOptions{Name: "IDX_" + v})
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func NewArango(addr string, auth string) (*ArangoStore, error) {
	ctx := context.Background()
	conn, err := http.NewConnection(http.ConnectionConfig{
		Endpoints: []string{addr},
		TLSConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	})
	if err != nil {
		return nil, err
	}
	config := driver.ClientConfig{
		Connection: conn,
	}
	splt := strings.Split(auth, ":")
	if len(splt) > 1 {
		config.Authentication = driver.BasicAuthentication(splt[0], splt[1])
	}
	client, err := driver.NewClient(config)
	if err != nil {
		return nil, err
	}
	has, err := client.DatabaseExists(ctx, "polygon-benchmark")
	if err != nil {
		return nil, err
	}
	if !has {
		_, err = client.CreateDatabase(ctx, "polygon-benchmark", nil)
		if err != nil {
			return nil, err
		}
	}
	db, err := client.Database(ctx, "polygon-benchmark")
	if err != nil {
		return nil, err
	}

	return &ArangoStore{
		Database: db,
		ctx:      ctx,
	}, nil
}
